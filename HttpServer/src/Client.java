import java.io.*;
import java.net.Socket;
import java.util.Scanner;

public class Client
{

    private static int fetchMessage(InputStream in)
    {
        byte buf[] = new byte[64 * 1024];
        int r = -1;
        try
        {
            r = in.read(buf);                       // read the whole message
            if (r <= 0)
            {
                return r;
            }

            String request = new String(buf, 0, r);
            System.out.println(request);
        }
        catch (IOException ex)
        {
            System.err.println(ex.toString());
        }
        return r;
    }


    public static void main(String[] args) throws InterruptedException
    {

        System.out.print("Enter hostname: ");
        Scanner stdIn = new Scanner(System.in);
        String hostname = stdIn.nextLine();


        try (
                Socket socket = new Socket(hostname, 80);
                PrintWriter out = new PrintWriter(socket.getOutputStream());
                BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()))
        )
        {
            String request = "POST / HTTP/1.1\r\n\r\n" +
                    "num=2&checksum=481f693417e9a74e783caea72063b606&data=c2FzaGE=";


            out.write(request);
            out.flush();
            fetchMessage(socket.getInputStream());

            in.close();
            out.close();
            socket.close();

        }
        catch (IOException e)
        {
            System.err.println(e.toString());
        }

    }

}
