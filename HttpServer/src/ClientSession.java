import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigInteger;
import java.net.Socket;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.StringTokenizer;

public class ClientSession implements Runnable
{
    protected Socket client;
    protected List<String> requestLines;
    protected int messageBodyStartIndex = -1;
    protected BigInteger checkSum = null;
    protected StringBuilder base64msg = null;
    protected int packageNum = -1;


    public ClientSession(Socket s)
    {
        client = s;
        requestLines = new ArrayList<>();
    }

    private void sendResponse(int statusCode, String phrase, String msg) throws IOException
    {
        StringBuilder response = new StringBuilder();
        response.append(String.format("HTTP/1.1 %d %s\r\n", statusCode, phrase));
        response.append("Content-Type: text/html\r\n");
        response.append("\r\n");
        response.append(String.format("<html><body><p>%s</p></body></html>", msg));

        OutputStream out = client.getOutputStream();
        byte[] resp = response.toString().getBytes();
        out.write(resp, 0, resp.length);
        out.flush();
        out.close();
    }

    private int fetchMessage()
    {
        StringBuilder line = new StringBuilder();
        byte buf[] = new byte[64 * 1024];
        int r = -1;
        try
        {
            requestLines.clear();
            messageBodyStartIndex = -1;
            InputStream in = client.getInputStream();
            r = in.read(buf);                       // read the whole message
            if (r <= 0)
            {
                return r;
            }

            String request = new String(buf, 0, r);
            System.out.println(request);
            for (int i = 0; i < r; i++)
            {
                if (request.charAt(i) != '\n' && request.charAt(i) != '\r')
                {
                    line.append(request.charAt(i));
                }
                else
                {
                    requestLines.add(line.toString());
                    i++;
                    if (line.toString().equals(""))
                    {
                        messageBodyStartIndex = requestLines.size();
                    }
                    line = new StringBuilder();
                }
            }
            if (!line.toString().equals(""))
            {
                requestLines.add(line.toString());
            }
        }
        catch (IOException ex)
        {
            System.err.println(ex.toString());
        }
        return r;
    }

    protected void closeStreams()
    {
        try
        {
            client.close();
        }
        catch (IOException ex)
        {
            System.err.println(ex.toString());
        }

    }

    protected boolean validate(String base64, BigInteger checkSum)
    {
        Base64.Decoder decoder = Base64.getDecoder();
        byte[] data = decoder.decode(base64);
        try
        {
            MessageDigest md5 = MessageDigest.getInstance("MD5");
            BigInteger check = new BigInteger(md5.digest(data));
            return check.equals(checkSum);
        }
        catch (NoSuchAlgorithmException ex)
        {
            System.err.println(ex.toString());
        }

        return false;
    }

    protected void getPostParameters()
    {
        StringTokenizer messageTokenizer = new StringTokenizer(requestLines.get(messageBodyStartIndex), "&");
        StringTokenizer paramTokenizer;
        String param;
        while (messageTokenizer.hasMoreTokens())
        {
            param = messageTokenizer.nextToken();
            paramTokenizer = new StringTokenizer(param, "=", true);
            switch (paramTokenizer.nextToken())
            {
                case "num":
                    paramTokenizer.nextToken();
                    packageNum = Integer.parseInt(paramTokenizer.nextToken());
                    break;
                case "checksum":
                    paramTokenizer.nextToken();
                    checkSum = new BigInteger(paramTokenizer.nextToken(), 16);
                    break;
                case "data":
                    paramTokenizer.nextToken();
                    base64msg = new StringBuilder();
                    while (paramTokenizer.hasMoreTokens())
                    {
                        base64msg.append(paramTokenizer.nextToken());
                    }
                    break;
            }

        }
    }

    @Override
    public void run()
    {
        try
        {
            while (fetchMessage() > 0)
            {
                try
                {
                    if (!requestLines.get(0).contains("POST"))
                    {
                        sendResponse(405, "Method Not Allowed", "Only POST method is available now.");
                        continue;
                    }
                    getPostParameters();

                }
                catch (IndexOutOfBoundsException ex)
                {
                    sendResponse(412, "Precondition Failed", "No message body");
                    continue;
                }

                if (validate(base64msg.toString(), checkSum))
                {
                    sendResponse(200, "OK", "OK");
                }
                else
                {
                    sendResponse(412, "Precondition Failed", "Check sum doesn't match.");
                }
            }
        }
        catch (Exception ex)
        {
            System.err.println(ex.toString());
        }
        finally
        {
            closeStreams();
        }
    }
}
