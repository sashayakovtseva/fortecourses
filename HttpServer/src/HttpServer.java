import java.net.*;
import java.util.Enumeration;

public class HttpServer
{
    private static final int HTTP_PORT = 80;

    private static InetAddress getLocalAddress()
    {
        try
        {
            Enumeration<NetworkInterface> interfaces = NetworkInterface.getNetworkInterfaces();
            while (interfaces.hasMoreElements())
            {
                for (InterfaceAddress f : interfaces.nextElement().getInterfaceAddresses())
                {
                    if (f.getAddress().isSiteLocalAddress())
                    {
                        return f.getAddress();
                    }
                }
            }
        }
        catch (SocketException e)
        {
            e.printStackTrace();
        }
        return null;
    }

    public static void main(String[] args)
    {

        InetAddress localAddress = getLocalAddress();
        if (localAddress == null)
        {
            System.err.println("No site local addresses. Exit...");
            System.exit(1);
        }

        try (ServerSocket serverSocket = new ServerSocket(HTTP_PORT, 0, localAddress))
        {
            System.out.println("Server started at " + localAddress.getHostAddress());
            while (true)
            {
                new Thread(new ClientSession(serverSocket.accept())).start();
            }
        }
        catch (Exception ex)
        {
            System.err.println(ex.toString());
            System.exit(1);
        }
    }


}
